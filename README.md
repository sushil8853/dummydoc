# Overview


Asun is Bangladesh cab booking app with availability of rides in accross Bangladesh. Empower your users and explore endless possibilities with the Asun APIs. Help your users find, book and track Asun rides with ease.

You can find reference documentation and guides to help you kick start integration of Asun into your app or website. Bring on the innovation and change the way of travels!

## Getting Started

    1. All REST request must be sent to `https://api.asun.com/v1/` using the `application/json` content type. Non-HTTPS will be redirect to HTTPS , possibly causing functional or performance issues with our application.

    2. All REST requests will result in a `200 Ok` response unless there is a server or infrastructure error. The API result will be wrapped in a JSON Result object, where the success field indicates response status.

    3. Data return is in ascending order.

    4. All time and timestamps is in milliseconds.


## HTTP Return Code

    1. HTTP 4XX return codes are used for malformed requests; the issue is on the sender's side.

    2.HTTP 403 return code is used when the WAF Limit (Web Application Firewall) has been violated.

    3.HTTP 429 return code is used when breaking a request rate limit (to many request).

    4.HTTP 418 return code is used when an IP has been auto-banned for continuing to send requests after receiving 429 codes.

    5.HTTP 5XX return codes are used for internal errors; the issue is on Asun's side. It is important to NOT treat this as a failure operation; the execution status is UNKNOWN and could have been a success.


## Error Response

```javascript
{
	"status":false,
	"statusCode":"M500",
	"msg":"Something happen wrong. Please try again"
}
```


## Authentication
___


### 1. Signup

    Method - POST
    Endpoint - /auth/signup
    Content-Type - application/json

Request Body

  | Name           |  Type  | Mandatory |
  | -------------- | :----: | --------: |
  | first_name     | String |       Yes |
  | last_name      | String |       Yes |
  | gender         | String |       Yes |
  | date_of_birth  | String |       Yes |
  | phone_no       | String |       Yes |
  | email          | String |       Yes |
  | profile_images | String |       Yes |
  | verify_code    | String |       Yes |
  | password       | String |       Yes |


  Response Body

   | Name            |  Type  | Mandatory |
   | --------------- | :----: | --------: |
   | phone_no        | String |       Yes |
   | referral        | String |       Yes |
   | message         | String |       Yes |
   | isPhoneVerified | String |       Yes |


 Response Example

  ```javascript
      {
        phone_no: "8801911291186",
        referral: "",
        message: "register successfully",
        sms_status: "",
    }
  ```


  ### 2. Login

  User can login with the help of phone number and emailId.

    Method - POST
    Endpoint - /auth/login
    Content-Type - application/json


 Request Body

  | Name     |  Type  | Mandatory |
  | -------- | :----: | --------: |
  | phone_no | String |       Yes |
  | email    | String |       Yes |
  | password | String |       Yes |


Response Body
```javascript
{
    token: "token",
    subscriber: "subscriber",
    sms_status: "sms_status",
    verify_code: "verify_code",
    history: [{
        type: 'point',
        loc: [],
        longitude: '',
        latitude: '',
        distance: '',
        is_recent: true,
        is_favourite: false
    }]
}
```


### 3. Forgot Password

  Reset you password with the help of phone number and otp

    Method - POST
    Endpoint - /auth/password
    Content-Type - application/json


Request Body

  | Name     |  Type  | Mandatory |
  | -------- | :----: | --------: |
  | phone_no | String |       Yes |
  | otp      | String |       Yes |
  | password | String |       Yes |


Response Body

  | Name    |  Type  | Mandatory |
  | ------- | :----: | --------: |
  | msg | String |       Yes |
  | status  | String |       Yes |
  | data    | Object |       Yes |

```javascript
{
    msg:"Password reset successfully",
    status:true,
    data:{}
}
```

### 4. Verify Password

This service will call inside the application. It'll use for password verification at the time of update password.

    Method - PUT
    Endpoint - /auth/password
    Content-Type - application/json
    Headers: {token:""}
    
Request Body

  | Name     |  Type  | Mandatory |
  | -------- | :----: | --------: |
  | password | String |       Yes |

Response Body

  | Name    |  Type  | Mandatory |
  | ------- | :----: | --------: |
  | msg | String |       Yes |
  | status  | String |       Yes |
  | data    | Object |       Yes |

```javascript
{
    msg:"Password is verified",
    status:true,
    data:{}
}
```


### 5. Change Password

  Update you password using existing password. 
  For change the current password user first verify his current password. When current password is valid then he'll call this service

    Method - PUT
    Endpoint - /auth/password
    Content-Type - application/json
    Headers: {token:""}
    

Request Body

  | Name             |  Type  | Mandatory |
  | ---------------- | :----: | --------: |
  | current_password | String |       Yes |
  | new_password     | String |       Yes |


Response Body

  | Name    |  Type  | Mandatory |
  | ------- | :----: | --------: |
  | msg | String |       Yes |
  | status  | String |       Yes |
  | data    | Object |       Yes |

```javascript
{
    msg:"Password updated successfully",
    status:true,
    data:{}
}
```


### 6. Send OTP

This service will be used for Send otp.

    Method - PUT
    Endpoint - /auth/phone/otp
    Content-Type - application/json
    Headers: {token:""}

Request Body

  | Name     |  Type  | Mandatory |
  | -------- | :----: | --------: |
  | phone_no | String |       Yes |


Response Body

  | Name    |  Type  | Mandatory |
  | ------- | :----: | --------: |
  | msg | String |       Yes |
  | status  | String |       Yes |
  | data    | Object |       Yes |

 Ex - Success response

```javascript
{
    msg:"OTP send successfully",
    status:true,
    data:{}
}
```

Ex - Failure response

```javascript
{
    msg:"Invalid phone number.",
    status:false,
    data:{}
}
```


### 7. Change Phone Number

Change user current phone number.

    Method - PUT
    Endpoint - /auth/phone
    Content-Type - application/json
    Headers: {token:""}
    

Request Body

  | Name         |  Type  | Mandatory |
  | ------------ | :----: | --------: |
  | phone_no     | String |       Yes |
  | new_phone_no | String |       Yes |
  | otp          | String |       Yes |


Response Body

  | Name    |  Type  | Mandatory |
  | ------- | :----: | --------: |
  | msg | String |       Yes |
  | status  | String |       Yes |
  | data    | Object |       Yes |

 Ex - Success response

```javascript
{
    msg:"Phone number updated successfully",
    status:true,
    data:{}
}
```

Ex - Failure response

```javascript
{
    msg:"Phone number already in use.",
    status:false,
    data:{}
}
```


### 8. Send Verification Email


    Method - POST
    Endpoint - /auth/email
    Content-Type - application/json
    Headers: {token:""}

Request Body

  | Name  |  Type  | Mandatory |
  | ----- | :----: | --------: |
  | email | String |       Yes |


Response Body

  | Name    |  Type  | Mandatory |
  | ------- | :----: | --------: |
  | msg | String |       Yes |
  | status  | String |       Yes |
  | data    | Object |       Yes |

 Ex - Success response

```javascript
{
    msg:"Verification email successfully",
    status:true,
    data:{}
}
```

Ex - Failure response

```javascript
{
    msg:"Something happen wrong. Please try again",
    status:false,
    data:{}
}
```


### 8. Verify Email

Verify user email id. For email verification, user just send a verification email.

    Method - GET
    Endpoint - /auth/email/verify
    Content-Type - application/json
    Headers: {}
    

Query Parameters

  | Name  |  Type  | Mandatory |
  | ----- | :----: | --------: |
  | token | String |       Yes |
  | email | String |       Yes |


Response Body

  | Name    |  Type  | Mandatory |
  | ------- | :----: | --------: |
  | msg | String |       Yes |
  | status  | String |       Yes |
  | data    | Object |       Yes |

```javascript
{
    msg:"Email is verified",
    status:true,
    data:{}
}
```

```javascript
{
    msg:"Invalid Token",
    status:false,
    data:{}
}
```


## User 
___

### 1. User Details

Get user profile details.
By default this service will return user profile data. If you want other details in return then you have to pass keys in query parameter.

    Method - GET
    Endpoint - /user
    Content-Type - application/json
    Headers: {token:""}

    Url Example -  {{baseUrl}}/user?p&rs&f
    

Query Parameters

  | Name |  Type  | Mandatory |     Description |
  | ---- | :----: | --------: | --------------: |
  | p    | String |        No |    User Profile |
  | rs   | String |        No | Recent searches |
  | t    | String |        No | Recent 10 trips |
  | x    | String |        No |        All Data |


Response Body 

  | Name    |  Type  | Mandatory |
  | ------- | :----: | --------: |
  | msg | String |       Yes |
  | status  | String |       Yes |
  | data    | Array  |       Yes |


```javascript
{
    "first_name": "name",
    "last_name": "last name",
    "referral": "referralCode",
    "email": "emailId,
    "phone_no": "phone_number",
    "account": "",
    "subscriber_type": "user",
    "created_at": date,
    "updated_at": date,
    "driver_info": 1,
    "is_active": 1,
    "fb_profile_imagess": 1,
    "profile_images": 1,
    "name": 1,
    "city": 1,
    "is_signup_completed": 1,
    "gender": 1,
    "date_of_birth": 1,
    "facebookId": 1,
    "fb_auth_token": 1,
    "full_name": 1
}
```

### 2. Update User

Update user basic details like profile name, image

    Method - PUT
    Endpoint - /user
    Content-Type - application/json
    Headers: {token:""}


Request Body 

  | Name          |  Type  | Mandatory |
  | ------------- | :----: | --------: |
  | full_name     | String |  Optional |
  | profile_image | String |  Optional |


Response Body

```javascript
{
    msg:'Profile updated successfully',
    status:true,
    data:{
        profile_image:"url",
        full_name:"updated name"
    }
}
```


## Trip
___

### 1. New Trip

Create new trip

    Method:POST
    Endpoint: /trip
    Content-Type - application/json
    Headers: {token:""}

Request Body 

  | Name          |  Type  | Mandatory |
  | ------------- | :----: | --------: |
  | address     | String |  Optional |
  | departure_location | String |  Optional |
  | destination_location | String |  Optional |
  | booking_date | String |  Optional |
  | booking_time | String |  Optional |

Response Body 

```javascript
    {
        message:'trip add successfully',
        trip_id:"1234"
    }
```

### 2. Get Trips

    Method:GET
    Endpoint: /trip
    Content-Type - application/json
    Headers: {token:""}


Response Body

```javascript
    {
        trip_date:1571134061459,
        payment_mode:'Cash',
        isCancelled:false,
        totalAmount:60,
        currency:'tk',
        trip_seq_id:'123',
        vehicle:"hyundai xcent",
        trip_rating:3.5,
        driver_profile_img:"",
        pickup:"",
        drop:""
    }
```


### 3. Get Trip details

    Method:GET
    Endpoint: /trip/:tripId
    Content-Type - application/json
    Headers: {token:""}


Response Body

```javascript
    {
        type: "",
        address: "",
        departure_address: "",
        destination_address: "",
        cancle_user: "",
        cancle_driver: "",
        departure_location:""
        destination_location:"",
        driver_id: "",
        passenger_id: "",
        booking_at: "",
        person: "",
        status: ""
    }
```


### 4. Cancel Trip
Cancel booked trip by user

    Method:PUT
    Endpoint: /trip/:tripId
    Content-Type - application/json
    Headers: {token:""}

    Status - "Accepted" || "onRide" || "Cancelled"



Request Body

  | Name          |  Type  | Mandatory |
  | ------------- | :----: | --------: |
  | Status     | String |  Yes |
  | price | String |  Optional |
  | killometre | String |  Optional |
  | longitude | String |  Optional |
  | latitude | String |  Optional |


Response Body

```javascript
    {
        type: "",
        address: "",
        departure_address: "",
        destination_address: "",
        coordinates: "",
        coordinates_date: "",
        departure_location: "",
        destination_location: "",
        departure_time: "",
        destination_time: "",
        driver_id: "",
        passenger_id: "",
        start_time: "",
        end_time: "",
        status: ""
    }
```

### 5. Accept / Reject Trip by Driver
Cancel booked trip by user

    Method:PUT
    Endpoint: /trip/:tripId
    Content-Type - application/json
    Headers: {token:""}




Response Body

```javascript
    {
        type: "",
        address: "",
        departure_address: "",
        destination_address: "",
        departure_location: "",
        destination_location: "",
        person: "",
        driver_id: "",
        passenger_id: "",
        booking_at: "",
        status: "cancelled",
    }
```

### 6. Trip Rating

Rate trip by user & driver both

    Method:POST
    Endpoint: /rating/:tripId
    Content-Type - application/json
    Headers: {token:""}


Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| rating     | String |  Optional |
| text | String |  Optional |


Response Body


Success Response

```javascript
{
    success:true,
    message:'Success Rating'
}
```


Failure Response

```javascript
{
    success:true,
    message:'All ready exist'
}
```


### 7. Trip Complaint

Create complaint against any trip

    Method:POST
    Endpoint: /trip/:tripId
    Content-Type - application/json
    Headers: {token:""}


Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| phone_no     | String |  Optional |
| text | String |  Yes |

Response Body

Success Response

```javascript
{
    success:true,
    phone_no:"4674347647673",
    text:"bad driving"
}
```

Failure Response

```javascript
{
    success:false,
    message:'Something happen wrong. Please try again'
}
```

### 8. Earnings

Get total earning details
    Method:POST
    Endpoint: /summary
    Content-Type - application/json
    Headers: {token:""}



Response Body

```javascript
{
        dashboard: {
        sum_fare: 0,
        sum_earnings: 0,
        sum_tips: 0,
        sum_instake_charge: 0,
        sum_discount: 0,
        sum_sub_total: 0,
        total_ride: 0,
        total_referral_earned: 0,
        total_ratings_count: 0,
        average_ratings: 0,
        one_star_count: 0,
        two_star_count: 0,
        three_star_count: 0,
        four_star_count: 0,
        five_star_count: 0,
        instake_charge_due: 0
    },
    today: {
        sum_fare: 0,
        sum_earnings: 0,
        sum_tips: 0,
        sum_instake_charge: 0,
        sum_discount: 0,
        sum_sub_total: 0,
        total_ride: 0,
        total_trip: {
            accepted: 0,
            canceled: 0,
            total_ride: 0
        },
    }

}
```


### 9. Dashboard

Get total earning details
    Method:POST
    Endpoint: /dashboard
    Content-Type - application/json
    Headers: {token:""}


Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| startdate     | String |  Optional |
| end_date | String |  Optional |


Response Body

```javascript
{
    _id: { day: '$day', month: '$month', year: '$year' },
    data: [{
        total_fare: 111,
        total_distance_fare: 111,
        total_distance: 11,
        total_discount: 11,
        sum_sub_total: 11,
        sum_total_time: 11,
        instake_charge: 11,
        tips_amount: 11,
        trips: [

            {
                _id: "$_id",
                instake_charge: "$instake_charge",
                start_time: "$start_time",
                end_time: "$end_time",
                payment_type: "$payment_type",
                status: "$status",
                passenger_id: "$passenger_id",
                distance: "$distance",
                duration: "$duration",
                unit: "$unit",
                base_fare_rate: "$base_fare_rate",
                total_fare: "$total_fare",
                discount: "$discount",
                sub_total: "$sub_total",
                time_fare: "$time_fare",
                payment_method: "$payment_method",
                departure_time: "$departure_time",
                avatar: "$avatar",
                distance_fare: "$distance_fare",
                trip_seq_id: "$trip_seq_id",
                destination_time: "$destination_time",
                rating: "$rating",
                tips_amount: "$tips_amount",
                address_pickup: "$pickup_name",
                address_dropoff: "$dropoff_name"
            }
        ]
    }],
}
```




## Car
___

### 1. car Type
Get all existing car type 

    Method:GET
    Endpoint: /cartype
    Content-Type - application/json
    Headers: {}


Response Body

```javascript
{
    name:"",
    image:{},
    killometre:Number,
    seats:4,
    base_fare:100,
    per_minit:Number,
    vehicle_type:"car",
    vehicle_type_name:"car",
    is_active:true,
    created_at:date
}
```

### 2. Vehicle search
Search vehicle by type

    Method:GET
    Endpoint: /vehicle-search
    Content-Type - application/json
    Headers: {}


Query Parameter

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| search     | String |  Optional |
| type | String |  Yes |


Response Body

```javascript
{
    vehicle:{
        name:"",
        image:{},
        killometre:Number,
        seats:4,
        base_fare:100,
        per_minit:Number,
        vehicle_type:"car",
        vehicle_type_name:"car",
        is_active:true,
        created_at:date
    },
    count:3
}
```


### 3. Vehicle search
Get all existing car type 

    Method:GET
    Endpoint: /cattype/:id
    Content-Type - application/json
    Headers: {}


Response Body

```javascript
{
    name:"",
    image:{},
    killometre:Number,
    seats:4,
    base_fare:100,
    per_minit:Number,
    vehicle_type:"car",
    vehicle_type_name:"car",
    is_active:true,
    created_at:date
}
```


## Coupon 
___

### 1. Apply Coupon

    Method:GET
    Endpoint: /apply-coupon
    Content-Type - application/json
    Headers: {}


Query Parameter

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| code     | String |  Yes |


Response Body

```javascript
{
    discount_amount:30
}
```

## Languages
___


### 1. Languages
Get application language

    Method:GET
    Endpoint: /languages
    Content-Type - application/json
    Headers: {}



Response Body

```javascript
{
    name:"hindi",
    code:"hi",
}
```

## Location
___

### 1. Add new location

    Method:POST
    Endpoint: /new-location-add
    Content-Type - application/json
    Headers: {}

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| longitude     | String |  Yes |
| latitude     | String |  Yes |
| country     | String |  Yes |
| city     | String |  Yes |
| area     | String |  Yes |
| address     | String |  Yes |


Response Body

```javascript
{
    coordinates: ,
    country: "India",
    city: "dhaka",
    area: "tangail",
    address:"mirzapur, tangail, dhaka" 
}
```


### 2. Service Availability
Check service availability in particular area

    Method:POST
    Endpoint: /Service Availability
    Content-Type - application/json
    Headers: {}


Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| longitude     | String |  Yes |
| latitude     | String |  Yes |


Response Body

```javascript
{
    longitude: 90.409187,
    latitude: 23.814494, 
    address: "mirzapur, tangail, dhaka"
}
```


### 3. Service Availability
Check service availability in particular area

    Method:GET
    Endpoint: /Service Availability
    Content-Type - application/json
    Headers: {}


Query Parameter

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| longitude     | String |  Optional |
| latitude     | String |  Optional |
| limit     | String |  Optional |
| distance      | String |  Optional |


Response Body

```javascript
{
    is_service_available_in_area:true,
    message:"",
    address:"mirzapur, tangail, dhaka"
}
```

### 4. Location Search

Search location cordinates according to pickup and drop location

    Method:GET
    Endpoint: /location-search
    Content-Type - application/json
    Headers: {}

Query Parameter

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| address_pickup     | String |  Optional |
| address_dropoff     | String |  Optional |


Response Body

```javascript
{
     pickup: [],
     dropoff: [],
    distance: {}
}
```

### 5. Location

Search location cordinates according to pickup and drop location

    Method:GET
    Endpoint: /location
    Content-Type - application/json
    Headers: {}

Query Parameter

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| phone_no     | String |  Optional |
| limit     | String |  Optional |
| distance     | String |  Optional |
| longitude     | String |  Optional |
| latitude     | String |  Optional |
| vehicle_type     | String |  Optional |
| vehicle_id     | String |  Optional |


Response Body

```javascript
{
   passenger:{phone_no:"3463474733"},
   driver_list:[
       {
           vehicle_info:'vehicle_id',
           driver:{
                _id: "location id",
                name: "driver location name",
                full_name: "driver name",
                phone_no: "phone number",
                email: "email id",
                rating: "rating",
                referral: "referral code",
                avatar: "profile image url",
                is_active: true,
                is_online: true,
           },
           driver_location:{ 
                longitude: 90.409187,
                latitude: 23.814494
               },
           distance:2
       }
   ]
}
```

### 6. Location
Update driver current location

    Method:PUT
    Endpoint: /location
    Content-Type - application/json
    Headers: {token:""}

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| longitude     | String |  Yes |
| latitude     | String |  Yes |


Response Body

```javascript
{
    latitude:123,
    longitude:2343
}
```

### 7. Location
Update driver current location

    Method:PUT
    Endpoint: /location/:id
    Content-Type - application/json
    Headers: {token:""}


Response Body

```javascript
{
    coordinates: [23,2323],
    type:"point"
}
```


## Search History
___

### 1. Search history

Get search hostory by user

    Method:GET
    Endpoint: /search-history
    Content-Type - application/json
    Headers: {token:""}

Query Parameter

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| startdate     | date |  Optional |
| end_date     | date |  Optional |
| page     | String |  Optional |


Response Body

```javascript
{
    latitude:90.409187,
    longitude:23.45723,
    distance:1,
    name:"dhaka",
    count:1,
    is_favourite:true
}
```

### 2. Search history

Add new search history
    Method:POST
    Endpoint: /search-history
    Content-Type - application/json
    Headers: {token:""}

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| longitude     | Number |  Yes |
| latitude     | Number |  Yes |
| name     | String |  Yes |
| favourite     | Boolean |  Optional |


Response Body

```javascript
{
    _id: "123",
    latitude:90.409187,
    longitude:23.45723,
    distance:1,
    name:"home",
    count:1,
    is_favourite:true,
    is_recent:true
}
```


### 3. Recent Search history
Update recent search history

    Method:PUT
    Endpoint: /recent-history/:id
    Content-Type - application/json
    Headers: {}

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| favourite     | Boolean |  Optional |


Response Body

```javascript
{
    latitude:90.409187,
    longitude:23.45723,
    distance:1,
    name:"home",
    is_favourite:true,
}
```

### 4. Delete Recent Search history
Update recent search history

    Method:DELETE
    Endpoint: /recent-history/:id
    Content-Type - application/json
    Headers: {}

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| favourite     | Boolean |  Optional |


Response Body

```javascript
{
    delete:true
}
```

### 5. Update favourite Search history
Update favourite search history

    Method:PUT
    Endpoint: /favourite-history/:id
    Content-Type - application/json
    Headers: {}

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| favourite     | Boolean |  Optional |


Response Body

```javascript
{
    latitude:90.409187,
    longitude:23.45723,
    distance:1,
    name:"home",
    is_favourite:true,
}
```

### 6. Delete favourite Search history
Remove favourite search history

    Method:DELETE
    Endpoint: /favourite-history/:id
    Content-Type - application/json
    Headers: {}

Response Body

```javascript
{
    delete:true
}
```

## Payment & Voucher

### 1. Payment History

    Method:GET
    Endpoint: /payment-history
    Content-Type - application/json
    Headers: {token:""}


Query Parameter

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| page     | String |  Optional |


Response Body

```javascript
{
    payment:[
        {
            v_no:"",
            v_type:"",
            v_date:"",
            coaid:"",
            coa_id_cccoa:"",
            status:"",
            bill_to_name:"",
            phone_no:"",
            subscriber_id:"",
            transaction_id:"",
            trip_id:"",
            narration:"",
            debit:"",
            credit:"",
            is_posted:"",
            is_approve:"",
            createdBy:"",
            updated_by:""
        }
    ],
    count:1
}
```

### 2. Payment
All payment history

    Method:GET
    Endpoint: /payment
    Content-Type - application/json
    Headers: {token:""}


Response Body

```javascript
{
    payment:[
        {
            departure_location: 22.32323,
            destination_location: 54.23232,
            driver_id: '123',
            price: 100
        }
        current_balance:100
    ]
}
```

### 3. Get single Payment details
Get payment detail by payment id

    Method:GET
    Endpoint: /payment/:payment_id
    Content-Type - application/json
    Headers: {token:""}


Response Body

```javascript
{    
    departure_location: 22.32323,
    destination_location: 54.23232,
    driver_id: '123',
    price: 100,
    current_balance:100
}
```

### 4. Add new payment for trip

Get payment detail by payment id

    Method:POST
    Endpoint: /payment/:payment_id
    Content-Type - application/json
    Headers: {token:""}

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| rating     | String |  Optional |
| text     | String |  Optional |


Response Body

```javascript
{     
    payment_amount: 100, 
    success: true,
    message: 'Success Payment'
 }
```

```javascript
{
    success: true,
    message: 'All ready payment' 
}
```

failure Response

```javascript
{
    success:false,
    message:"Something happen wrong"
}
```


### 5. Instake Payment

Get payment detail by payment id

    Method:POST
    Endpoint: /payment-instake
    Content-Type - application/json
    Headers: {token:""}

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| amount     | String |  Yes |


Response Body

```javascript
{ 
    payment_amount: 10,
    success: true,
    message: 'Success Payment'
}
```



## SOCKETS
___


### 1. Login

    Url - {baseUrl}/login

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| status     | String |  Yes |
| socket_id     | String |  Yes |
| phone_no     | String |  Yes |


Response Body

```javascript
{
    online: 2,
    offline: 1,
    onride: onride,
    phone_no: "2232323",
    history_divice: [{
        phone_no:"",
        sub_array:[]
    }],
    device: {
        device_type:"",
        device_name:"",
        publicIp_v4:"",
        publicIp_v6:"",
        host:"",
        origin:"",
        referer:"",
        user_agent:""
    }
}
```


### 2. Location Search

    Url - {baseUrl}/location-search

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| phone_no     | String |  Optional |
| limit     | String |  Optional |
| phone_no     | String |  Optional |
| latitude     | String |  Optional |
| longitude     | String |  Optional |
| vehicle_type     | String |  Optional |
| vehicle_id     | String |  Optional |

Response Body

```javascript
{
    location:
    {
        location_name: "india",
        email: "sushil@xyz.com",
        loc: [],
        phone_no: "8853474345",
        is_online: true,
        subscriber_type: "basic",
        vehicle_type: "",
        vehicle_id: "",
        referral: "",
        full_name: "",
        longitude: "",
        latitude: "",
        rating: 5,
        name: "",
        is_active: true,
        profile_images: ""
    },
    distance:{
        duration:3,
        duration_unit:"hr"
    }
}
```


### 3. Tip amount

    Url - {baseUrl}/on-tips-addedd

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| driver     | String |  Yes |
| type     | String |  Yes |
| tips.trip_id     | String |  Yes |
| tips.tips_amount     | String |  Yes |


```javascript
{
    driver:"",
    type:"new-message",
    tips:{
        trip_id:"",
        tips_amount:20
    }
}
```

### 4. Tip amount

    Url - {baseUrl}/get_system_status

Smjh se pare hai iska code


### 5. Tip amount

    Url - {baseUrl}/send-ride-request


### 6. Accept

    Url - {baseUrl}/accept

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| status     | String |  Yes |
| _id     | String |  Yes |

Response Body

```javascript
{
    status:"accept",
    _id:"",
    trip:{
        status:"",
        message:""
    }
}
```

### 7. Start ride

    Url - {baseUrl}/start-ride

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| status     | String |  Yes |
| trip._id     | String |  Yes |

Response Body

```javascript
{
    trip:{
        status:"onRide",
        message:""
    },
    passanger:{ phone_no:"" },
    driver:{ phone_no:"" }
}
```

### 8. End ride

    Url - {baseUrl}/end-ride

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| status     | String |  Yes |
| trip._id     | String |  Yes |

Response Body

```javascript
{
    trip:{
        status:"endRide",
        message:""
    },
    passanger:{ phone_no:"" },
    driver:{ phone_no:"" }
}
```


### 9. Cancel ride request by driver

    Url - {baseUrl}/cancel_ride_request_driver

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| status     | String |  Yes |
| trip_canceled     | String |  Yes |
| driver_id     | String |  Yes |

Response Body

```javascript
{
    trip:{
        status:"cancelled",
        message:""
    },
    message:"",
    passanger:{ passenger_id:"" },
    driver:{ driver_id:"" }
}
```

### 10. Cancel ride request by passenger

    Url - {baseUrl}/cancel_ride_request_passenger

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| status     | String |  Yes |
| trip_canceled     | String |  Yes |
| driver_id     | String |  Yes |

Response Body

```javascript
{
    trip:{
        status:"cancelled",
        message:""
    },
    message:"",
    passanger:{ passenger_id:"" },
    driver:{ driver_id:"" }
}
```

### 11. Get passenger system status

    Url - {baseUrl}/get_passenger_system_status


Response Body

```javascript
{
    phone_no: "",
    trip: {},
    distance: 2,
    system_update_info: {
        "version_code": 1,
        "version_name": "1.0.1",

        "force_update": {
            "is_enabled": false,
            "message": "New version APK is downloading",
            "apk_url": "http://instake.com/app/instake.apk"
        },

        "alert_dialog": {
            "is_enabled": true,
            "cancelable": false,
            "title": "Update Available!",
            "body": "There is an update available in google play. Please update your app to experience better service!",
            "action_text": "Ok",
            "action_url": "https://play.google.com/store/apps/details?id=com.instake.captain"
        },
        "contact_us": {
            "hotline": "01623994640",
            "message": "Attend our training session and get online in minutes! Please come to our office or Call us",
        },
    }
}
```


### 10. Cancel ride request by passenger

    Url - {baseUrl}/get_single_driver_location

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| phone_no     | String |  Yes |

Response Body

```javascript
{
    current_location:{
        lattitude:23.4343445,
        longitude:43.434545
    },
    driver_info:{
        name:"",
        phone_no:""
    }
}
```

### 10. Add new location

    Url - {baseUrl}/add-new-location

Request Body

| Name          |  Type  | Mandatory |
| ------------- | :----: | --------: |
| driver_phone_no     | String |  Yes |

Response Body

```javascript
{
    driver_phone_no:"",
    message:""
}
```


